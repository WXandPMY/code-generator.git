package com.wx.exam.service;

import com.wx.exam.service.module.Result;
import com.wx.exam.vo.QuestionJudgeVO;

/** 
 * <br/>
 * Created by wangxiao on 2018/07/22
 */
public interface QuestionJudgeService {

	Result addQuestionJudge(QuestionJudgeVO questionJudgeVO) throws Exception;

	Result updateQuestionJudge(QuestionJudgeVO questionJudgeVO) throws Exception;
	
	Result findDetailQuestionJudge(QuestionJudgeVO questionJudgeVO) throws Exception;
	
	Result listQuestionJudge(QuestionJudgeVO questionJudgeVO) throws Exception;

    Result listQuestionJudgePage(QuestionJudgeVO questionJudgeVO) throws Exception;
	
	Result countQuestionJudge(QuestionJudgeVO questionJudgeVO) throws Exception;
	
	Result deleteQuestionJudge(QuestionJudgeVO questionJudgeVO) throws Exception;
}