package com.wx.exam.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.wx.exam.service.module.Result;
import com.wx.exam.service.QuestionJudgeService;
import com.wx.exam.vo.QuestionJudgeVO;
import com.wx.exam.dao.mapper.QuestionJudgeMapper;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/** 
 * <br/>
 * Created by wangxiao on 2018/07/22
 */
@Service("questionJudgeService")
public class QuestionJudgeServiceImpl implements QuestionJudgeService {

	private final static Logger LOG = LoggerFactory.getLogger(QuestionJudgeServiceImpl.class);

	@Resource
	private QuestionJudgeMapper questionJudgeMapper;

	@Override
	public Result addQuestionJudge(QuestionJudgeVO questionJudgeVO) throws Exception {
		return null;
	}

	@Override
	public Result updateQuestionJudge(QuestionJudgeVO questionJudgeVO) throws Exception {
		return null;
	}
	
	@Override
	public Result findDetailQuestionJudge(QuestionJudgeVO questionJudgeVO) throws Exception{
		return null;
	}

	@Override
	public Result listQuestionJudge(QuestionJudgeVO questionJudgeVO) throws Exception{
		return null;
	}
	
	@Override
	public Result listQuestionJudgePage(QuestionJudgeVO questionJudgeVO) throws Exception{
		return null;
	}
	
	@Override
	public Result countQuestionJudge(QuestionJudgeVO questionJudgeVO) throws Exception{
		return null;
	}
	
	@Override
	public Result deleteQuestionJudge(QuestionJudgeVO questionJudgeVO) throws Exception{
		return null;
	}

}