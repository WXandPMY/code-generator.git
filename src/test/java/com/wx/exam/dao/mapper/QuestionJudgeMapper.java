package com.wx.exam.dao.mapper;

import java.util.List;

import com.wx.exam.vo.QuestionJudgeVO;
import com.wx.exam.data.QuestionJudgeDO;
import org.springframework.stereotype.Repository;

/** 
 * <br/>
 * Created by wangxiao on 2018/07/22
 */
@Repository
public interface QuestionJudgeMapper {

    Integer addQuestionJudge(QuestionJudgeVO questionJudgeVO);

    Integer updateQuestionJudge(QuestionJudgeVO questionJudgeVO);

    QuestionJudgeDO findDetailQuestionJudge(QuestionJudgeVO questionJudgeVO);

    List<QuestionJudgeDO> listQuestionJudge(QuestionJudgeVO questionJudgeVO);

    List<QuestionJudgeDO> listQuestionJudgePage(QuestionJudgeVO questionJudgeVO);

    Integer countQuestionJudge(QuestionJudgeVO questionJudgeVO);

    Integer deleteQuestionJudge(QuestionJudgeVO questionJudgeVO);

}
